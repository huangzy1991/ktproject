package com.example.ktproject.ext

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.toast(toastStr: String) =
    Toast.makeText(this, toastStr, Toast.LENGTH_SHORT).show()